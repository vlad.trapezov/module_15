#include <iostream>


void FindOddNumbers(int Limit, bool IsOdd)
{
    for (int i = IsOdd; i <= Limit; i += 2)
    {
        std::cout << i << " ";
    }

    std::cout << "\n";
}



int main()
{
    const int N = 2023;

    std::cout << "List of odd numbers: \n";
    FindOddNumbers(N, true);

    std::cout << "\n";

    std::cout << "List of even numbers: \n";
    FindOddNumbers(N, false);
}


